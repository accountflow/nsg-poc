package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.dto.PeriodCoveredDTO;
import com.balances.nsgpoc.factory.EntryHeaderFactory;
import com.balances.nsgpoc.factory.XbrlFactory;
import com.balances.nsgpoc.model.gl.ObjectFactory;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.xbrl._2003.instance.Xbrl;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class ReportManagerTest {

  @InjectMocks ReportManager reportManager = new ReportManager();

  @Mock private CompanyManager companyManager;
  @Mock private XbrlFactory xbrlFactory;
  @Mock private EntryHeaderFactory entryHeaderFactory;
  @Mock private TransactionNSGManager transactionManager;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void emptyDocumentTest() throws JAXBException, SAXException {
    StringWriter writer = new StringWriter();
    when(companyManager.getCompany(1L)).thenReturn(Optional.empty());

    ObjectFactory objectFactory = new ObjectFactory();
    XbrlFactory xbrlFactoryObject = new XbrlFactory();
    xbrlFactoryObject.setObjectFactory(objectFactory);
    final XbrliXbrl xbrliXbrl = xbrlFactoryObject.createXbrl();
    when(xbrlFactory.createXbrl()).thenReturn(xbrliXbrl);
    XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo documentInfo =
            xbrlFactoryObject.createGlCorDocumentInfo();
    when(xbrlFactory.createGlCorDocumentInfo()).thenReturn(documentInfo);

    PeriodCoveredDTO dates = new PeriodCoveredDTO(Optional.of(new Date()), Optional.of(new Date()));

    when(transactionManager.getMinMaxDateByCompany(1L)).thenReturn(dates);

    XbrliXbrl result = reportManager.getReportForCompany(1L, Optional.empty());

    Marshaller marshaller = getXbrliMarshaller();
    marshaller.marshal(result, writer);

    System.out.println(writer.toString());
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    String currentDate = format.format(new Date());
    String expectedResult = ReportResults.EMPTY_RESULT.replace("2020-03-16", currentDate);
    assertEquals(expectedResult, writer.toString());
  }

  private static final Class<?>[] FILING_TYPES = new Class[] {
          Xbrl.class,
          org.xbrl._2003.linkbase.Linkbase.class,
          org.xbrl._2003.instance.ObjectFactory.class,
          org.xbrl._2003.linkbase.FootnoteLink.class,
  };

  private static Marshaller getXbrliMarshaller() throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(XbrliXbrl.class);
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    return marshaller;
  }

  private static Marshaller getXbrl2Marshaller() throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(FILING_TYPES);
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    return marshaller;
  }


  @Test
  public void testNewXbrl() throws JAXBException {
    Xbrl xbrl2 = new Xbrl();
    StringWriter writer = new StringWriter();

    Marshaller marshaller = getXbrl2Marshaller();
    marshaller.marshal(xbrl2, writer);

    System.out.println(writer.toString());

    assertTrue(true);
  }
}
