//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.06 at 12:22:27 AM EET 
//


package org.xbrl._int.gl.bus._2016_12_01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xbrl._int.gl.gen._2016_12_01.ActiveItemType;
import org.xbrl._int.gl.usk._2016_12_01.JobCodeItemType;
import org.xbrl._int.gl.usk._2016_12_01.JobDescriptionItemType;
import org.xbrl._int.gl.usk._2016_12_01.JobPhaseCodeItemType;
import org.xbrl._int.gl.usk._2016_12_01.JobPhaseDescriptionItemType;


/**
 * <p>Java class for jobInfoComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="jobInfoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/usk/2016-12-01}jobCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/usk/2016-12-01}jobDescription" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/usk/2016-12-01}jobPhaseCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/usk/2016-12-01}jobPhaseDescription" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/usk/2016-12-01}jobActive" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jobInfoComplexType", propOrder = {
    "jobCode",
    "jobDescription",
    "jobPhaseCode",
    "jobPhaseDescription",
    "jobActive"
})
public class JobInfoComplexType {

    @XmlElement(namespace = "http://www.xbrl.org/int/gl/usk/2016-12-01", nillable = true)
    protected JobCodeItemType jobCode;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/usk/2016-12-01", nillable = true)
    protected JobDescriptionItemType jobDescription;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/usk/2016-12-01", nillable = true)
    protected JobPhaseCodeItemType jobPhaseCode;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/usk/2016-12-01", nillable = true)
    protected JobPhaseDescriptionItemType jobPhaseDescription;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/usk/2016-12-01", nillable = true)
    protected ActiveItemType jobActive;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the jobCode property.
     * 
     * @return
     *     possible object is
     *     {@link JobCodeItemType }
     *     
     */
    public JobCodeItemType getJobCode() {
        return jobCode;
    }

    /**
     * Sets the value of the jobCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobCodeItemType }
     *     
     */
    public void setJobCode(JobCodeItemType value) {
        this.jobCode = value;
    }

    /**
     * Gets the value of the jobDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JobDescriptionItemType }
     *     
     */
    public JobDescriptionItemType getJobDescription() {
        return jobDescription;
    }

    /**
     * Sets the value of the jobDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDescriptionItemType }
     *     
     */
    public void setJobDescription(JobDescriptionItemType value) {
        this.jobDescription = value;
    }

    /**
     * Gets the value of the jobPhaseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JobPhaseCodeItemType }
     *     
     */
    public JobPhaseCodeItemType getJobPhaseCode() {
        return jobPhaseCode;
    }

    /**
     * Sets the value of the jobPhaseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobPhaseCodeItemType }
     *     
     */
    public void setJobPhaseCode(JobPhaseCodeItemType value) {
        this.jobPhaseCode = value;
    }

    /**
     * Gets the value of the jobPhaseDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JobPhaseDescriptionItemType }
     *     
     */
    public JobPhaseDescriptionItemType getJobPhaseDescription() {
        return jobPhaseDescription;
    }

    /**
     * Sets the value of the jobPhaseDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobPhaseDescriptionItemType }
     *     
     */
    public void setJobPhaseDescription(JobPhaseDescriptionItemType value) {
        this.jobPhaseDescription = value;
    }

    /**
     * Gets the value of the jobActive property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveItemType }
     *     
     */
    public ActiveItemType getJobActive() {
        return jobActive;
    }

    /**
     * Sets the value of the jobActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveItemType }
     *     
     */
    public void setJobActive(ActiveItemType value) {
        this.jobActive = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
