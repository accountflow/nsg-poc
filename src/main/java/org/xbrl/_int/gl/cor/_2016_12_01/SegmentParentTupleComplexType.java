//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.06 at 12:22:27 AM EET 
//


package org.xbrl._int.gl.cor._2016_12_01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for segmentParentTupleComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="segmentParentTupleComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}parentSubaccountCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}parentSubaccountType" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}reportingTreeIdentifier" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}parentSubaccountProportion" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "segmentParentTupleComplexType", propOrder = {
    "parentSubaccountCode",
    "parentSubaccountType",
    "reportingTreeIdentifier",
    "parentSubaccountProportion"
})
public class SegmentParentTupleComplexType {

    @XmlElement(nillable = true)
    protected ParentSubaccountCodeItemType parentSubaccountCode;
    @XmlElement(nillable = true)
    protected ParentSubaccountTypeItemType parentSubaccountType;
    @XmlElement(nillable = true)
    protected ReportingTreeIdentifierItemType reportingTreeIdentifier;
    @XmlElement(nillable = true)
    protected ParentSubaccountProportionItemType parentSubaccountProportion;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the parentSubaccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link ParentSubaccountCodeItemType }
     *     
     */
    public ParentSubaccountCodeItemType getParentSubaccountCode() {
        return parentSubaccountCode;
    }

    /**
     * Sets the value of the parentSubaccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentSubaccountCodeItemType }
     *     
     */
    public void setParentSubaccountCode(ParentSubaccountCodeItemType value) {
        this.parentSubaccountCode = value;
    }

    /**
     * Gets the value of the parentSubaccountType property.
     * 
     * @return
     *     possible object is
     *     {@link ParentSubaccountTypeItemType }
     *     
     */
    public ParentSubaccountTypeItemType getParentSubaccountType() {
        return parentSubaccountType;
    }

    /**
     * Sets the value of the parentSubaccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentSubaccountTypeItemType }
     *     
     */
    public void setParentSubaccountType(ParentSubaccountTypeItemType value) {
        this.parentSubaccountType = value;
    }

    /**
     * Gets the value of the reportingTreeIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingTreeIdentifierItemType }
     *     
     */
    public ReportingTreeIdentifierItemType getReportingTreeIdentifier() {
        return reportingTreeIdentifier;
    }

    /**
     * Sets the value of the reportingTreeIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingTreeIdentifierItemType }
     *     
     */
    public void setReportingTreeIdentifier(ReportingTreeIdentifierItemType value) {
        this.reportingTreeIdentifier = value;
    }

    /**
     * Gets the value of the parentSubaccountProportion property.
     * 
     * @return
     *     possible object is
     *     {@link ParentSubaccountProportionItemType }
     *     
     */
    public ParentSubaccountProportionItemType getParentSubaccountProportion() {
        return parentSubaccountProportion;
    }

    /**
     * Sets the value of the parentSubaccountProportion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentSubaccountProportionItemType }
     *     
     */
    public void setParentSubaccountProportion(ParentSubaccountProportionItemType value) {
        this.parentSubaccountProportion = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
