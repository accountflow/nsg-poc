package org.xbrl._int.gl.cor._2016_12_01;

import org.xbrl._2003.instance.StringItemType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "language")
public class Language
        extends StringItemType
{


}