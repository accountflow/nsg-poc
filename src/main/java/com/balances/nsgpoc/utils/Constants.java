package com.balances.nsgpoc.utils;

public final class Constants {

  private Constants() {}

  public static final String XBRL_MEDIA_TYPE = "application/xbrl-instance+xml";
  public static final String DEFAULT_CONTEXT_REF = "now";

  public static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";
  public static final String XBRLL = "http://www.xbrl.org/2003/linkbase";
  public static final String XBRLI = "http://www.xbrl.org/2003/instance";
  public static final String XLINK = "http://www.w3.org/1999/xlink";
  public static final String GL_COR = "http://www.xbrl.org/int/gl/cor/2016-12-01";
  public static final String GL_BUS = "http://www.xbrl.org/int/gl/bus/2016-12-01";
  public static final String GL_MUC = "http://www.xbrl.org/int/gl/muc/2016-12-01";

}
