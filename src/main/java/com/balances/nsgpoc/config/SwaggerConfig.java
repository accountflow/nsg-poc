package com.balances.nsgpoc.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
  private String BASE_PACKAGE = "com.balances.nsgpoc.controller";

  @Bean
  public Docket eDesignApi(SwaggerConfigProperties swaggerConfigProperties) {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo(swaggerConfigProperties))
        .enable(Boolean.parseBoolean(swaggerConfigProperties.getEnabled()))
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(Predicates.not(PathSelectors.regex("/actuator")))
        .paths(Predicates.not(PathSelectors.regex("/actuator/info")))
        .paths(Predicates.not(PathSelectors.regex("/error.*")))
        .build()
        .pathMapping("/")
        .directModelSubstitute(LocalDate.class, String.class)
        .genericModelSubstitutes(ResponseEntity.class)
        .useDefaultResponseMessages(swaggerConfigProperties.getUseDefaultResponseMessages())
        .enableUrlTemplating(swaggerConfigProperties.getEnableUrlTemplating());
  }

  @Bean
  UiConfiguration uiConfig(SwaggerConfigProperties swaggerConfigProperties) {
    return UiConfigurationBuilder.builder()
        .deepLinking(swaggerConfigProperties.getDeepLinking())
        .displayOperationId(swaggerConfigProperties.getDisplayOperationId())
        .defaultModelsExpandDepth(
            Integer.valueOf(swaggerConfigProperties.getDefaultModelsExpandDepth()))
        .defaultModelExpandDepth(
            Integer.valueOf(swaggerConfigProperties.getDefaultModelExpandDepth()))
        .defaultModelRendering(ModelRendering.EXAMPLE)
        .displayRequestDuration(swaggerConfigProperties.getDisplayRequestDuration())
        .docExpansion(DocExpansion.NONE)
        .filter(swaggerConfigProperties.getFilter())
        .maxDisplayedTags(Integer.valueOf(swaggerConfigProperties.getMaxDisplayedTags()))
        .operationsSorter(OperationsSorter.ALPHA)
        .showExtensions(swaggerConfigProperties.getShowExtensions())
        .tagsSorter(TagsSorter.ALPHA)
        .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
        .validatorUrl(null)
        .build();
  }

  private ApiInfo apiInfo(SwaggerConfigProperties swaggerConfigProperties) {
    return new ApiInfoBuilder()
        .title(swaggerConfigProperties.getTitle())
        .description(swaggerConfigProperties.getDescription())
        .version(swaggerConfigProperties.getApiVersion())
        .build();
  }
}
