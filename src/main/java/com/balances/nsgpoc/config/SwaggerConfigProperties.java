package com.balances.nsgpoc.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration("swaggerConfigProperties")
public class SwaggerConfigProperties {

  private String apiVersion = "1.0";

  @Value("${swagger.enabled}")
  private String enabled = "false";

  @Value("${swagger.title}")
  private String title;

  @Value("${swagger.description}")
  private String description;

  private Boolean useDefaultResponseMessages = false;

  private Boolean enableUrlTemplating = false;

  private Boolean deepLinking = true;

  private String defaultModelsExpandDepth = "1";

  private String defaultModelExpandDepth = "1";

  private Boolean displayOperationId = false;

  private Boolean displayRequestDuration = false;

  private Boolean filter = false;

  private String maxDisplayedTags = "0";

  private Boolean showExtensions = false;

}
