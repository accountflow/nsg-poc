package com.balances.nsgpoc.repository;

import com.balances.nsgpoc.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {}
