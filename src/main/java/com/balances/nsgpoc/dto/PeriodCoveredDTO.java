package com.balances.nsgpoc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Optional;

@Data
@Getter
@Setter
@AllArgsConstructor
public class PeriodCoveredDTO {
    private Optional<Date> minOptional;
    private Optional<Date> maxOptional;
}
