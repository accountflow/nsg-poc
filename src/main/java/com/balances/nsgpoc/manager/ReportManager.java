package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.dto.PeriodCoveredDTO;
import com.balances.nsgpoc.factory.EntryHeaderFactory;
import com.balances.nsgpoc.factory.XbrlFactory;
import com.balances.nsgpoc.model.Company;
import com.balances.nsgpoc.model.CompanyTransactionDocument;
import com.balances.nsgpoc.model.Line;
import com.balances.nsgpoc.model.Transaction;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static com.balances.nsgpoc.utils.Constants.DEFAULT_CONTEXT_REF;

@Service
@Transactional(readOnly = true)
public class ReportManager {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportManager.class);

  private static String ACCOUNT_TYPE = "account";
  private static String DECIMALS_AFTER_POINT = "2";
  private static String DEBIT_CODE = "D";
  private static String CREDIT_CODE = "C";
  private static String CURRENCY_NOK = "nok";
  private static String POSTING_STATUS = "posted";
  private static String VOUCHER = "voucher";

  private static String XLINK = "../plt/case-c-b-m/gl-plt-all-2016-12-01.xsd";
  private static String ARCROLE = "http://www.w3.org/1999/xlink/properties/linkbase";
  private static String XLINK_TYPE = "simple";

  @Autowired private CompanyManager companyManager;
  @Autowired private XbrlFactory xbrlFactory;
  @Autowired private EntryHeaderFactory entryHeaderFactory;
  @Autowired private TransactionNSGManager transactionManager;

  @Autowired private DocumentManager documentManager;

  public XbrliXbrl getReportForCompany(
      Long companyId, Optional<Date> startDay, Optional<Date> endDay) {

    CompanyTransactionDocument dto = documentManager.createDocumentForCompany(companyId, startDay, endDay);

    var xbrl = xbrlFactory.createXbrl();

    setLinkSchema(xbrl);
    setXbrliUnit(xbrl);

    xbrl.getGlCorAccountingEntries().setGlCorDocumentInfo(xbrlFactory.createGlCorDocumentInfo());

    PeriodCoveredDTO dates = transactionManager.getMinMaxDateByCompany(companyId);

    if (startDay.isPresent()) {
      dates.setMinOptional(startDay);
    }
    if (endDay.isPresent()) {
      dates.setMaxOptional(endDay);
    }

    setPeriodCovered(xbrl, dates);

    xbrl.getGlCorAccountingEntries()
        .setGlCorEntityInformation(xbrlFactory.createGlCorEntityInformation(dto.getCompanyOptional()));

    if (Objects.nonNull(dto.getTransactions())) {
      List<Transaction> transactions = dto.getTransactions();
      transactions.forEach(
          t -> {
            XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader entryHeader =
                entryHeaderFactory.createEntryHeader(t);
            xbrl.getGlCorAccountingEntries().getGlCorEntryHeader().add(entryHeader);

            List<Line> lines = t.getLine();

            AtomicInteger counter = new AtomicInteger(1);
            for (Line line : lines) {

              XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail entryDetail =
                  entryHeaderFactory.createEntryDetail();
              entryDetail.getGlCorLineNumber().setLineNumber(counter.get());
              entryDetail.getGlCorLineNumber().setContextRef(DEFAULT_CONTEXT_REF);
              entryHeader.getGlCorEntryDetail().add(entryDetail);

              XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
                  account = entryHeaderFactory.createEmptyAccount();

              updateAccount(account, line);
              entryDetail.setGlCorAccount(account);

              updateEntryDetail(entryDetail, line);
              counter.incrementAndGet();
            }
          });
    }
    return xbrl;
  }

  public XbrliXbrl getReportForCompany(Long companyId, Optional<String> transactionIdOptional) {

    Optional<Company> companyOptional = companyManager.getCompany(companyId);
    List<Transaction> transactions;

    Date start = new Date(0L);
    Date finish = new Date();

    if (transactionIdOptional.isEmpty()) {
      transactions =
          transactionManager.findTransactionsForDefaultCustomer(companyId, start, finish);
    } else {
      transactions =
          transactionManager.findTransactionsByCustomerIdAndTransactionID(
              companyId, transactionIdOptional.get());
    }
    PeriodCoveredDTO dates = transactionManager.getMinMaxDateByCompany(companyId);

    var xbrl = xbrlFactory.createXbrl();

    setLinkSchema(xbrl);
    setXbrliUnit(xbrl);

    xbrl.getGlCorAccountingEntries().setGlCorDocumentInfo(xbrlFactory.createGlCorDocumentInfo());

    setPeriodCovered(xbrl, dates);
    xbrl.getGlCorAccountingEntries()
        .setGlCorEntityInformation(xbrlFactory.createGlCorEntityInformation(companyOptional));

    if (Objects.nonNull(transactions)) {
      transactions.forEach(
          t -> {
            XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader entryHeader =
                entryHeaderFactory.createEntryHeader(t);
            xbrl.getGlCorAccountingEntries().getGlCorEntryHeader().add(entryHeader);

            List<Line> lines = t.getLine();

            AtomicInteger counter = new AtomicInteger(1);
            for (Line line : lines) {

              XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail entryDetail =
                  entryHeaderFactory.createEntryDetail();
              entryDetail.getGlCorLineNumber().setLineNumber(counter.get());
              entryDetail.getGlCorLineNumber().setContextRef(DEFAULT_CONTEXT_REF);
              entryHeader.getGlCorEntryDetail().add(entryDetail);

              XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
                  account = entryHeaderFactory.createEmptyAccount();

              updateAccount(account, line);
              entryDetail.setGlCorAccount(account);

              updateEntryDetail(entryDetail, line);
              counter.incrementAndGet();
            }
          });
    }
    return xbrl;
  }

  private void setPeriodCovered(XbrliXbrl xbrl, PeriodCoveredDTO dates) {
    xbrl.getGlCorAccountingEntries()
        .getGlCorDocumentInfo()
        .setGlCorPeriodCoveredStart(
            new XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo.GlCorPeriodCoveredStart());
    xbrl.getGlCorAccountingEntries()
        .getGlCorDocumentInfo()
        .getGlCorPeriodCoveredStart()
        .setPeriodCoveredStart(dates.getMinOptional().orElse(new Date()));

    xbrl.getGlCorAccountingEntries()
        .getGlCorDocumentInfo()
        .getGlCorPeriodCoveredStart()
        .setContextRef(DEFAULT_CONTEXT_REF);

    xbrl.getGlCorAccountingEntries()
        .getGlCorDocumentInfo()
        .getGlCorPeriodCoveredEnd()
        .setPeriodCoveredEnd(dates.getMaxOptional().orElse(new Date()));
    ;
  }

  private void updateAccount(
      XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount account,
      Line line) {
    account.getGlCorAccountMainID().setAccountMainID(line.getAccountID());
    account.getGlCorAccountMainID().setContextRef(DEFAULT_CONTEXT_REF);

    account.getGlCorAccountMainDescription().setAccountMainDescription("Rent");
    account.getGlCorAccountMainDescription().setContextRef(DEFAULT_CONTEXT_REF);

    account.getGlCorAccountPurposeCode().setAccountPurposeCode("usgaap");
    account.getGlCorAccountPurposeCode().setContextRef(DEFAULT_CONTEXT_REF);

    account.getGlCorAccountType().setAccountType(ACCOUNT_TYPE);
    account.getGlCorAccountType().setContextRef(DEFAULT_CONTEXT_REF);
  }

  private void updateEntryDetail(
      XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail entryDetail, Line line) {

    entryDetail.getGlCorAmount().setDecimals(DECIMALS_AFTER_POINT);
    entryDetail.getGlCorAmount().setAmount(BigDecimal.ZERO);
    entryDetail.getGlCorAmount().setContextRef(DEFAULT_CONTEXT_REF);
    if (Objects.nonNull(line.getCreditAmount())
        && Objects.nonNull(line.getCreditAmount().getAmount())) {
      BigDecimal amount = line.getCreditAmount().getAmount();
      entryDetail.getGlCorAmount().setAmount(amount);
      entryDetail.getGlCorAmount().setContextRef(DEFAULT_CONTEXT_REF);
      entryDetail.getGlCorDebitCreditCode().setDebitCreditCode(CREDIT_CODE);
      entryDetail.getGlCorDebitCreditCode().setContextRef(DEFAULT_CONTEXT_REF);
    } else if (Objects.nonNull(line.getDebitAmount())
        && Objects.nonNull(line.getDebitAmount().getAmount())) {
      BigDecimal amount = line.getDebitAmount().getAmount();
      entryDetail.getGlCorAmount().setAmount(amount);
      entryDetail.getGlCorAmount().setContextRef(DEFAULT_CONTEXT_REF);
      entryDetail.getGlCorDebitCreditCode().setDebitCreditCode(DEBIT_CODE);
      entryDetail.getGlCorDebitCreditCode().setContextRef(DEFAULT_CONTEXT_REF);
    }
    entryDetail.getGlCorAmount().setUnitRef(CURRENCY_NOK);
    entryDetail.getGlCorAmount().setContextRef(DEFAULT_CONTEXT_REF);

    entryDetail.getGlCorPostingDate().setPostingDate(line.getSystemEntryTime());
    entryDetail.getGlCorPostingDate().setContextRef(DEFAULT_CONTEXT_REF);

    entryDetail.getGlCorPostingStatus().setPostingStatus(POSTING_STATUS);
    entryDetail.getGlCorPostingStatus().setContextRef(DEFAULT_CONTEXT_REF);

    entryDetail.getGlCorDocumentType().setDocumentType(VOUCHER);
    entryDetail.getGlCorDocumentType().setContextRef(DEFAULT_CONTEXT_REF);
  }

  private void setXbrliUnit(XbrliXbrl xbrl) {
    xbrl.setXbrliUnit(new XbrliXbrl.XbrliUnit());
    xbrl.getXbrliUnit().setId("NotUsed");
    xbrl.getXbrliUnit().setXbrliMeasure("pure");
  }

  private void setLinkSchema(XbrliXbrl xbrl) {
    xbrl.setLinkSchemaRef(new XbrliXbrl.LinkSchemaRef());
    xbrl.getLinkSchemaRef().setXlinkHref(XLINK);
    xbrl.getLinkSchemaRef().setArcrole(ARCROLE);
    xbrl.getLinkSchemaRef().setXlinkType(XLINK_TYPE);
  }
}
