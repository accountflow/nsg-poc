package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.model.Company;
import com.balances.nsgpoc.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class CompanyManager {

  @Autowired private CompanyRepository companyRepository;

  public Optional<Company> getCompany(Long companyId) {
    return this.companyRepository.findById(companyId);
  }
}
