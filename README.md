# nsg-poc

Nordic Smart Government - Proof of Concept

- url: 'https://nsg.test.accountflow.net'
    description: The test server
- url: 'https://nsg.test.accountflow.net/swagger-ui.html'
    description: The Swagger UI page on test server